﻿namespace kucing
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timerScanHook = new System.Windows.Forms.Timer(this.components);
            this.groupBoxHook = new System.Windows.Forms.GroupBox();
            this.labelDebugCursorHook = new System.Windows.Forms.Label();
            this.pictureBoxDebugHook = new System.Windows.Forms.PictureBox();
            this.groupBoxHookMan = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.trackBarDensity = new System.Windows.Forms.TrackBar();
            this.labelDensity = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.trackBarDivider = new System.Windows.Forms.TrackBar();
            this.labelDivider = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelLevel2 = new System.Windows.Forms.Label();
            this.trackBarLevel2 = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelLevel1 = new System.Windows.Forms.Label();
            this.trackBarLevel1 = new System.Windows.Forms.TrackBar();
            this.buttonHook = new System.Windows.Forms.Button();
            this.pictureBoxHook = new System.Windows.Forms.PictureBox();
            this.groupBoxCast = new System.Windows.Forms.GroupBox();
            this.labelDebugCount = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.trackBarConvolution = new System.Windows.Forms.TrackBar();
            this.labelConvolution = new System.Windows.Forms.Label();
            this.groupBoxHook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDebugHook)).BeginInit();
            this.groupBoxHookMan.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDensity)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDivider)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLevel2)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLevel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHook)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarConvolution)).BeginInit();
            this.SuspendLayout();
            // 
            // timerScanHook
            // 
            this.timerScanHook.Tick += new System.EventHandler(this.timerScanHook_Tick);
            // 
            // groupBoxHook
            // 
            this.groupBoxHook.Controls.Add(this.labelDebugCount);
            this.groupBoxHook.Controls.Add(this.labelDebugCursorHook);
            this.groupBoxHook.Controls.Add(this.pictureBoxDebugHook);
            this.groupBoxHook.Controls.Add(this.groupBoxHookMan);
            this.groupBoxHook.Controls.Add(this.buttonHook);
            this.groupBoxHook.Controls.Add(this.pictureBoxHook);
            this.groupBoxHook.Location = new System.Drawing.Point(12, 12);
            this.groupBoxHook.Name = "groupBoxHook";
            this.groupBoxHook.Size = new System.Drawing.Size(293, 402);
            this.groupBoxHook.TabIndex = 3;
            this.groupBoxHook.TabStop = false;
            this.groupBoxHook.Text = "Hook Sensor";
            // 
            // labelDebugCursorHook
            // 
            this.labelDebugCursorHook.AutoSize = true;
            this.labelDebugCursorHook.Location = new System.Drawing.Point(28, 307);
            this.labelDebugCursorHook.Name = "labelDebugCursorHook";
            this.labelDebugCursorHook.Size = new System.Drawing.Size(19, 13);
            this.labelDebugCursorHook.TabIndex = 12;
            this.labelDebugCursorHook.Text = "20";
            // 
            // pictureBoxDebugHook
            // 
            this.pictureBoxDebugHook.Location = new System.Drawing.Point(31, 266);
            this.pictureBoxDebugHook.Name = "pictureBoxDebugHook";
            this.pictureBoxDebugHook.Size = new System.Drawing.Size(38, 38);
            this.pictureBoxDebugHook.TabIndex = 6;
            this.pictureBoxDebugHook.TabStop = false;
            // 
            // groupBoxHookMan
            // 
            this.groupBoxHookMan.Controls.Add(this.groupBox5);
            this.groupBoxHookMan.Controls.Add(this.groupBox4);
            this.groupBoxHookMan.Controls.Add(this.groupBox3);
            this.groupBoxHookMan.Controls.Add(this.groupBox2);
            this.groupBoxHookMan.Controls.Add(this.groupBox1);
            this.groupBoxHookMan.Location = new System.Drawing.Point(112, 19);
            this.groupBoxHookMan.Name = "groupBoxHookMan";
            this.groupBoxHookMan.Size = new System.Drawing.Size(172, 373);
            this.groupBoxHookMan.TabIndex = 5;
            this.groupBoxHookMan.TabStop = false;
            this.groupBoxHookMan.Text = "Manual Settings";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.trackBarDensity);
            this.groupBox4.Controls.Add(this.labelDensity);
            this.groupBox4.Location = new System.Drawing.Point(12, 228);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(150, 65);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Minimum Density";
            // 
            // trackBarDensity
            // 
            this.trackBarDensity.Location = new System.Drawing.Point(6, 19);
            this.trackBarDensity.Maximum = 9;
            this.trackBarDensity.Name = "trackBarDensity";
            this.trackBarDensity.Size = new System.Drawing.Size(104, 42);
            this.trackBarDensity.TabIndex = 12;
            this.trackBarDensity.Scroll += new System.EventHandler(this.trackBarDensity_Scroll);
            // 
            // labelDensity
            // 
            this.labelDensity.AutoSize = true;
            this.labelDensity.Location = new System.Drawing.Point(116, 16);
            this.labelDensity.Name = "labelDensity";
            this.labelDensity.Size = new System.Drawing.Size(19, 13);
            this.labelDensity.TabIndex = 11;
            this.labelDensity.Text = "20";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.trackBarDivider);
            this.groupBox3.Controls.Add(this.labelDivider);
            this.groupBox3.Location = new System.Drawing.Point(12, 161);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(150, 65);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sensitivity";
            // 
            // trackBarDivider
            // 
            this.trackBarDivider.Location = new System.Drawing.Point(6, 19);
            this.trackBarDivider.Maximum = 8;
            this.trackBarDivider.Name = "trackBarDivider";
            this.trackBarDivider.Size = new System.Drawing.Size(104, 42);
            this.trackBarDivider.TabIndex = 11;
            this.trackBarDivider.Value = 4;
            this.trackBarDivider.Scroll += new System.EventHandler(this.trackBarDivider_Scroll);
            // 
            // labelDivider
            // 
            this.labelDivider.AutoSize = true;
            this.labelDivider.Location = new System.Drawing.Point(116, 16);
            this.labelDivider.Name = "labelDivider";
            this.labelDivider.Size = new System.Drawing.Size(22, 13);
            this.labelDivider.TabIndex = 10;
            this.labelDivider.Text = "0.5";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelLevel2);
            this.groupBox2.Controls.Add(this.trackBarLevel2);
            this.groupBox2.Location = new System.Drawing.Point(12, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 65);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Threshold";
            // 
            // labelLevel2
            // 
            this.labelLevel2.AutoSize = true;
            this.labelLevel2.Location = new System.Drawing.Point(120, 19);
            this.labelLevel2.Name = "labelLevel2";
            this.labelLevel2.Size = new System.Drawing.Size(19, 13);
            this.labelLevel2.TabIndex = 9;
            this.labelLevel2.Text = "10";
            // 
            // trackBarLevel2
            // 
            this.trackBarLevel2.Location = new System.Drawing.Point(10, 19);
            this.trackBarLevel2.Name = "trackBarLevel2";
            this.trackBarLevel2.Size = new System.Drawing.Size(104, 42);
            this.trackBarLevel2.TabIndex = 8;
            this.trackBarLevel2.Value = 1;
            this.trackBarLevel2.Scroll += new System.EventHandler(this.trackBarLevel2_Scroll);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelLevel1);
            this.groupBox1.Controls.Add(this.trackBarLevel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 65);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Levels";
            // 
            // labelLevel1
            // 
            this.labelLevel1.AutoSize = true;
            this.labelLevel1.Location = new System.Drawing.Point(120, 19);
            this.labelLevel1.Name = "labelLevel1";
            this.labelLevel1.Size = new System.Drawing.Size(19, 13);
            this.labelLevel1.TabIndex = 10;
            this.labelLevel1.Text = "60";
            // 
            // trackBarLevel1
            // 
            this.trackBarLevel1.Location = new System.Drawing.Point(10, 19);
            this.trackBarLevel1.Maximum = 20;
            this.trackBarLevel1.Name = "trackBarLevel1";
            this.trackBarLevel1.Size = new System.Drawing.Size(104, 42);
            this.trackBarLevel1.TabIndex = 9;
            this.trackBarLevel1.Value = 5;
            this.trackBarLevel1.Scroll += new System.EventHandler(this.trackBarLevel1_Scroll);
            // 
            // buttonHook
            // 
            this.buttonHook.Location = new System.Drawing.Point(5, 231);
            this.buttonHook.Name = "buttonHook";
            this.buttonHook.Size = new System.Drawing.Size(100, 23);
            this.buttonHook.TabIndex = 4;
            this.buttonHook.Text = "Reposition";
            this.buttonHook.UseVisualStyleBackColor = true;
            this.buttonHook.Click += new System.EventHandler(this.buttonHook_Click);
            // 
            // pictureBoxHook
            // 
            this.pictureBoxHook.Location = new System.Drawing.Point(6, 19);
            this.pictureBoxHook.Name = "pictureBoxHook";
            this.pictureBoxHook.Size = new System.Drawing.Size(100, 200);
            this.pictureBoxHook.TabIndex = 3;
            this.pictureBoxHook.TabStop = false;
            // 
            // groupBoxCast
            // 
            this.groupBoxCast.Location = new System.Drawing.Point(348, 31);
            this.groupBoxCast.Name = "groupBoxCast";
            this.groupBoxCast.Size = new System.Drawing.Size(238, 279);
            this.groupBoxCast.TabIndex = 4;
            this.groupBoxCast.TabStop = false;
            this.groupBoxCast.Text = "CastSensor";
            // 
            // labelDebugCount
            // 
            this.labelDebugCount.AutoSize = true;
            this.labelDebugCount.Location = new System.Drawing.Point(75, 266);
            this.labelDebugCount.Name = "labelDebugCount";
            this.labelDebugCount.Size = new System.Drawing.Size(19, 13);
            this.labelDebugCount.TabIndex = 12;
            this.labelDebugCount.Text = "20";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.trackBarConvolution);
            this.groupBox5.Controls.Add(this.labelConvolution);
            this.groupBox5.Location = new System.Drawing.Point(12, 299);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(150, 65);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Convolution Multiplier";
            // 
            // trackBarConvolution
            // 
            this.trackBarConvolution.Location = new System.Drawing.Point(6, 19);
            this.trackBarConvolution.Maximum = 6;
            this.trackBarConvolution.Name = "trackBarConvolution";
            this.trackBarConvolution.Size = new System.Drawing.Size(104, 42);
            this.trackBarConvolution.TabIndex = 12;
            this.trackBarConvolution.Value = 3;
            this.trackBarConvolution.Scroll += new System.EventHandler(this.trackBarConvolution_Scroll);
            // 
            // labelConvolution
            // 
            this.labelConvolution.AutoSize = true;
            this.labelConvolution.Location = new System.Drawing.Point(116, 16);
            this.labelConvolution.Name = "labelConvolution";
            this.labelConvolution.Size = new System.Drawing.Size(13, 13);
            this.labelConvolution.TabIndex = 11;
            this.labelConvolution.Text = "1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 421);
            this.Controls.Add(this.groupBoxCast);
            this.Controls.Add(this.groupBoxHook);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Aku Memancing";
            this.groupBoxHook.ResumeLayout(false);
            this.groupBoxHook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDebugHook)).EndInit();
            this.groupBoxHookMan.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDensity)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDivider)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLevel2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLevel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHook)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarConvolution)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerScanHook;
        private System.Windows.Forms.GroupBox groupBoxHook;
        private System.Windows.Forms.GroupBox groupBoxHookMan;
        private System.Windows.Forms.Button buttonHook;
        private System.Windows.Forms.PictureBox pictureBoxHook;
        private System.Windows.Forms.GroupBox groupBoxCast;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TrackBar trackBarDensity;
        private System.Windows.Forms.Label labelDensity;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TrackBar trackBarDivider;
        private System.Windows.Forms.Label labelDivider;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelLevel2;
        private System.Windows.Forms.TrackBar trackBarLevel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelLevel1;
        private System.Windows.Forms.TrackBar trackBarLevel1;
        private System.Windows.Forms.PictureBox pictureBoxDebugHook;
        private System.Windows.Forms.Label labelDebugCursorHook;
        private System.Windows.Forms.Label labelDebugCount;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TrackBar trackBarConvolution;
        private System.Windows.Forms.Label labelConvolution;
    }
}

