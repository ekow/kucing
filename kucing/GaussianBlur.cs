﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Drawing.Imaging;

namespace kucing
{
    [Serializable]
    public class GaussianBlur
    {
        private int _radius = 6;
        private int[] _kernel;
        private int _kernelSum;
        private int[,] _multable;

        public GaussianBlur()
        {
            PreCalculateSomeStuff();
        }

        public GaussianBlur(int radius)
        {
            _radius = radius;
            PreCalculateSomeStuff();
        }

        private void PreCalculateSomeStuff()
        {
            int sz = _radius * 2 + 1;
            _kernel = new int[sz];
            _multable = new int[sz, 256];
            for (int i = 1; i <= _radius; i++)
            {
                int szi = _radius - i;
                int szj = _radius + i;
                _kernel[szj] = _kernel[szi] = (szi + 1) * (szi + 1);
                _kernelSum += (_kernel[szj] + _kernel[szi]);
                for (int j = 0; j < 256; j++)
                {
                    _multable[szj, j] = _multable[szi, j] = _kernel[szj] * j;
                }
            }
            _kernel[_radius] = (_radius + 1) * (_radius + 1);
            _kernelSum += _kernel[_radius];
            for (int j = 0; j < 256; j++)
            {
                _multable[_radius, j] = _kernel[_radius] * j;
            }
        }

        public Bitmap ProcessImage(Bitmap inputImage)
        {
            Bitmap src = inputImage;
            Bitmap dst = new Bitmap(src);

            BitmapData dstData = dst.LockBits(
                new Rectangle(0, 0, dst.Width, dst.Height),
                ImageLockMode.ReadWrite, dst.PixelFormat);

            int pixelCount = src.Width * src.Height;
            int[] b = new int[pixelCount];
            int[] g = new int[pixelCount];
            int[] r = new int[pixelCount];

            int[] b2 = new int[pixelCount];
            int[] g2 = new int[pixelCount];
            int[] r2 = new int[pixelCount];


            int offset = dstData.Stride - src.Width * 4;
            int index = 0;
            unsafe
            {
                byte*[,] dstPtr = new byte*[dst.Width, dst.Height];
                byte* ptr = (byte*)dstData.Scan0.ToPointer();
                for (int i = 0; i < src.Height; i++)
                {
                    for (int j = 0; j < src.Width; j++, ptr += 4)
                    {
                        dstPtr[j, i] = ptr;
                        b[index] = ptr[0];
                        g[index] = ptr[1];
                        r[index] = ptr[2];

                        ++index;
                    }
                    ptr += offset;
                }

                int bsum;
                int gsum;
                int rsum;
                int sum;
                int read;
                int start = 0;
                index = 0;

                // vertical
                /*
                for (int i = 0; i < src.Height; i++)
                {
                    for (int j = 0; j < src.Width; j++)
                    {
                        bsum = gsum = rsum = sum = 0;
                        read = index - _radius;

                        for (int z = 0; z < _kernel.Length; z++)
                        {
                            if (read >= start && read < start + src.Width)
                            {
                                bsum += _multable[z, b[read]];
                                gsum += _multable[z, g[read]];
                                rsum += _multable[z, r[read]];
                                sum += _kernel[z];
                            }
                            ++read;
                        }

                        b2[index] = (bsum / sum);
                        g2[index] = (gsum / sum);
                        r2[index] = (rsum / sum);

                        ++index;
                    }
                    start += src.Width;
                }
                */
                // horizontal
                int tempy;
                for (int i = 0; i < src.Height; i++)
                {
                    int y = i - _radius;
                    start = y * src.Width;
                    for (int j = 0; j < src.Width; j++)
                    {
                        bsum = gsum = rsum = sum = 0;
                        read = start + j;
                        tempy = y;
                        for (int z = 0; z < _kernel.Length; z++)
                        {
                            if (tempy >= 0 && tempy < src.Height)
                            {
                                bsum += _multable[z, b[read]];
                                gsum += _multable[z, g[read]];
                                rsum += _multable[z, r[read]];

                                //bsum += _multable[z, b2[read]];
                                //gsum += _multable[z, g2[read]];
                                //rsum += _multable[z, r2[read]];
                                sum += _kernel[z];
                            }
                            read += src.Width;
                            ++tempy;
                        }

                        byte* pcell = dstPtr[j, i];
                        pcell[0] = (byte)(bsum / sum);
                        pcell[1] = (byte)(gsum / sum);
                        pcell[2] = (byte)(rsum / sum);
                    }
                }
            }
            
            dst.UnlockBits(dstData);
            return dst;
        }
    }
}