﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace kucing
{
    public partial class Form1 : Form
    {
        private Boolean mCursorHook = false;
        private Point mHookArea = new Point();
        private Point mBobblePoint = new Point();
        private Point mLevelEnhance = new Point(60, 10);
        private float mDivider = 0.5f;
        private float mConvolution = 1.0f;
        private Int32 mDensity = 20;
        private Boolean mBobbleFound = false;

        public Form1()
        {
            InitializeComponent();
            this.Text = "Aku Memancing v" + Application.ProductVersion.ToString() + " [0x" + this.Handle.ToString("X") + "]";
            timerScanHook.Enabled = true;
            labelDebugCursorHook.Text = "(" + mHookArea.X + ", " + mHookArea.Y + ")";
        }

        private Bitmap EnhanceImage(Bitmap source)
        {
            if (source == null) return source;
            if (source.PixelFormat != PixelFormat.Format32bppArgb) return source;

            int width = source.Width;
            int height = source.Height;

            // create new image
            Bitmap target = new Bitmap(source);

            // lock source bitmap data
            BitmapData srcData = source.LockBits(
                new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, source.PixelFormat);

            // lock destination bitmap data
            BitmapData dstData = target.LockBits(
                new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, target.PixelFormat);

            int offset = dstData.Stride - width * 4;
            float scale = (float)Math.Sqrt(width * height);

            // do the job
            unsafe
            {
                byte* src = (byte*)srcData.Scan0.ToPointer();
                byte* ptr = (byte*)dstData.Scan0.ToPointer();

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++, ptr += 4)
                    {
                        int avt = 0;
                        int avc = 0;
                        float avf = 0.0f;
                        for (int i = -1; i <= 1; i++)
                        {
                            for (int j = -1; j <= 1; j++)
                            {
                                int yy = y + i;
                                int xx = x + i;

                                if (yy < 0 || yy >= height)
                                    j = 1;
                                else if (xx >= width)
                                    j = 1;
                                else if (xx >= 0)
                                {
                                    byte* p = &src[yy * srcData.Stride + xx * 4];
                                    // get pixel attribute
                                    byte pxB = (byte)p[0];
                                    byte pxG = (byte)p[1];
                                    byte pxR = (byte)p[2];

                                    int range = Math.Abs(x - xx) + Math.Abs(y - yy);
                                    //float conv = 2.0f; // angka dari langit
                                    //if (range > 0) conv = conv / (2 * range);
                                    float conv = 1;
                                    if (mConvolution > 1)
                                        conv = (2 - range) * mConvolution;
                                    else if (mConvolution < 0)
                                        conv = range * mConvolution * -1;
                                    if (conv <= 0) conv = 1;
                                    avt += (byte)(conv * (pxB + pxG + pxR) / 3);
                                    avc++;
                                    avf += conv;
                                }
                            }
                        }
                        //byte avg = (byte)((1 * avt) / (avc * 1));
                        byte avg = (byte)((1 * avt) / (avf * 1));
                        /*
                        // get pixel attribute
                        byte pxB = (byte)src[0];
                        byte pxG = (byte)src[1];
                        byte pxR = (byte)src[2];
                        
                        // simple grayscaling
                        byte avg = (byte) ((pxB + pxG + pxR) / 3);
                        */
                        ///*
                        // levels 100-120
                        byte tLo = (byte)mLevelEnhance.X;
                        byte tHi = (byte)(mLevelEnhance.X + mLevelEnhance.Y);
                        if (avg < tLo) avg = tLo;
                        if (avg > tHi) avg = tHi;
                        byte lev = (byte)((avg - tLo) * 255 / (tHi - tLo));
                        
                        // apply to image
                        ptr[0] = lev;
                        ptr[1] = lev;
                        ptr[2] = lev;
                        //*/
                        /*
                        ptr[0] = avg;
                        ptr[1] = avg;
                        ptr[2] = avg;
                        */
                    }
                    ptr += offset;
                    //src += offset;
                }
            }
            // unlock destination images
            source.UnlockBits(srcData);
            target.UnlockBits(dstData);

            return target;
        }

        private Bitmap FindBobble(Bitmap source)
        {
            Point result = new Point(-1, -1);

            if (source == null) return source;
            if (source.PixelFormat != PixelFormat.Format32bppArgb) return source;

            int width = source.Width;
            int height = source.Height;

            // create new image
            Bitmap target = new Bitmap(source);

            // lock destination bitmap data
            BitmapData dstData = target.LockBits(
                new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, source.PixelFormat);

            int offset = dstData.Stride - width * 4;
            float scale = (float)Math.Sqrt(width * height);

            Int64 pointXColl = 0;
            Int64 pointYColl = 0;
            Int32 pointCount = 0;
            
            // do the job
            unsafe
            {
                byte* ptr = (byte*)dstData.Scan0.ToPointer();

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++, ptr += 4)
                    {
                        // get pixel attribute
                        byte pxB = (byte)ptr[0];
                        byte pxG = (byte)ptr[1];
                        byte pxR = (byte)ptr[2];
                        byte pxA = (byte)ptr[3];
                        byte avg = (byte)((pxB + pxG + pxR) / 3);
                        byte tDivider = (byte)((1 - mDivider) * 255);

                        //collect
                        if (avg > (tDivider))
                        {
                            pointXColl += x;
                            pointYColl += y;
                            pointCount++;

                            // kasih warna
                            ptr[0] = 160;
                            ptr[1] = 60;
                            ptr[2] = 250;
                        }
                    }
                    ptr += offset;
                }
            }
            // unlock destination images
            target.UnlockBits(dstData);

            //calc
            labelDebugCount.Text = pointCount.ToString();
            if (mDensity > 0 && pointCount > mDensity && pointCount < mDensity + 400)
            {
                result.X = (Int32)(pointXColl / pointCount);
                result.Y = (Int32)(pointYColl / pointCount);
            }

            mBobblePoint = result;

            return target;
        }

        private void timerScanHook_Tick(object sender, EventArgs e)
        {
            if (mCursorHook)
            {
                mHookArea = Cursor.Position;
                labelDebugCursorHook.Text = "(" + mHookArea.X + ", " + mHookArea.Y + ")";
            }
            Bitmap bmp = new Bitmap(100, 200, PixelFormat.Format32bppArgb);
            Graphics pgfx = Graphics.FromImage(bmp); //pictureBox1.CreateGraphics();
            pgfx.CopyFromScreen(mHookArea.X, mHookArea.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
            Bitmap enh;
            //enh = bmp;
            enh = EnhanceImage(bmp);
            //enh = mGBlur.ProcessImage(bmp);
            if (enh != null)
            {
                Graphics pdbggfx = pictureBoxDebugHook.CreateGraphics();
                enh = FindBobble(enh);
                if (mBobblePoint.X > 0 && mBobblePoint.Y > 0)
                {
                    Graphics pgfx2 = Graphics.FromImage(enh);
                    Pen red = new Pen(Color.Red);
                    Pen blue = new Pen(Color.Blue);
                    //pgfx2.DrawEllipse(red, mBobblePoint.X - 5, mBobblePoint.Y + 5, 10, 10);
                    pgfx2.FillRectangle(blue.Brush, mBobblePoint.X - 2, mBobblePoint.Y + 9, 3, 3);
                    pdbggfx.FillRectangle(red.Brush, 0, 0, pictureBoxDebugHook.Width, pictureBoxDebugHook.Height);
                }
                else
                {
                    Pen white = new Pen(Color.White);
                    pdbggfx.FillRectangle(white.Brush, 0, 0, pictureBoxDebugHook.Width, pictureBoxDebugHook.Height);
                }
                pictureBoxHook.Image = (Image)enh;
            }
            System.GC.Collect(); // free mem
            //Application.DoEvents();
        }

        private void buttonHook_Click(object sender, EventArgs e)
        {
            mCursorHook = !mCursorHook;
            if (mCursorHook)
                buttonHook.Text = "Press spacebar";
            else
                buttonHook.Text = "Reposition";
        }

        private void trackBarLevel1_Scroll(object sender, EventArgs e)
        {
            mLevelEnhance.X = trackBarLevel1.Value * 12;
            if (mLevelEnhance.X > 240) mLevelEnhance.X = 250;
            if ((mLevelEnhance.X + mLevelEnhance.Y) > 255)
            {
                mLevelEnhance.Y = 255 - mLevelEnhance.X;
                trackBarLevel2.Value = (mLevelEnhance.Y / 5) - 1;
            }
            labelLevel1.Text = mLevelEnhance.X.ToString();
            labelLevel2.Text = mLevelEnhance.Y.ToString();
        }

        private void trackBarLevel2_Scroll(object sender, EventArgs e)
        {
            mLevelEnhance.Y = (trackBarLevel2.Value + 1) * 5;
            if ((mLevelEnhance.X + mLevelEnhance.Y) > 255)
            {
                mLevelEnhance.Y = 255 - mLevelEnhance.X;
                trackBarLevel2.Value = (mLevelEnhance.Y / 5) - 1;
            }
            labelLevel2.Text = mLevelEnhance.Y.ToString();
        }

        private void trackBarDivider_Scroll(object sender, EventArgs e)
        {
            mDivider = (trackBarDivider.Value + 1) * 0.1f;
            labelDivider.Text = mDivider.ToString();
        }

        private void trackBarDensity_Scroll(object sender, EventArgs e)
        {
            mDensity = (trackBarDensity.Value + 1) * 20;
            labelDensity.Text = mDensity.ToString();
        }

        private void trackBarConvolution_Scroll(object sender, EventArgs e)
        {
            mConvolution = trackBarConvolution.Value - 2;
            if (mConvolution < 1) mConvolution -= 2;
            labelConvolution.Text = mConvolution.ToString();
        }
    }
}
